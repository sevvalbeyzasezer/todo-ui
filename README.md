# modanisa-todo

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

## Deployment to cloud
```
docker build --build-arg VUE_APP_API_URL=http://34.118.91.77:8081 -t gcr.io/red-planet-312108/modanisa-todo:0.0.2 .
docker push gcr.io/red-planet-312108/modanisa-todo:0.0.2
```

## Run docker image on local
```
docker run -p 8080:8080 --rm --name modanisa-todo gcr.io/red-planet-312108/modanisa-todo:0.0.2
```
